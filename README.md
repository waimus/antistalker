# Antistalker
a game by waimus

Built with Godot Engine 4.0 beta 6.

November 2022.

![img](./textures/overview_all.jpg)
<center>
	level overviews
</center>

## How to load project file
 1. Extract the project directory and note the "project.godot" file
 2. Retrieve Godot Engine 4.0 beta 6 from https://downloads.tuxfamily.org/godotengine/4.0/beta6/
 3. Download "Godot_v4.0-beta6_win64.exe.zip" (52.3 mb)
 4. Extract and launch the editor
 5. Load the project by pointing out the "project.godot" file on the project's directory where you've extracted

## License
Mozilla Public License 2.0 

see `LICENSE` for further detail.