extends Node3D


var target_count = 5
var disposed_objects: Array[RigidBody3D] = []

@onready var progress: ProgressBar = $Container/ProgressBar


func _ready() -> void:
	progress.max_value = target_count


func _physics_process(delta: float) -> void:
	progress.value = lerp(
			progress.value, 
			float(disposed_objects.size()), 
			1 * delta)
	
	if progress.value > target_count - 0.1:
		get_tree().change_scene_to_file("res://nodes/ui/menu_main.tscn")


func _on_area_3d_body_entered(body: Node3D) -> void:
	if body is RigidBody3D:
		disposed_objects.append(body)


func _on_area_3d_body_exited(body: Node3D) -> void:
	if body is RigidBody3D:
		var id: int = disposed_objects.find(body)
		disposed_objects.remove_at(id)
