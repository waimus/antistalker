extends Control


@onready
var label_fps: Label = Label.new()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	label_fps.anchor_right = ANCHOR_END
	label_fps.anchor_bottom = ANCHOR_END
	self.add_child(label_fps)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	label_fps.set_text(str(Engine.get_frames_per_second()))


func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.is_action_pressed("r_debug_fps"):
			label_fps.set_visible(!label_fps.is_visible_in_tree())
