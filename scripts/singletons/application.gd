extends Node
# application.gd
# Defines basic global application functions


var menu_ingame: Control
var stop_scale: float = ProjectSettings.get_setting("game/stop_time_scale")
var play_scale: float = ProjectSettings.get_setting("game/play_time_scale")


func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		# Quit application/pause menu shortcut
		if event.is_action_pressed("ui_cancel"):
			menu_ingame = get_tree().get_first_node_in_group("MENU_INGAME")
			if menu_ingame:
				if !menu_ingame.is_visible_in_tree():
					Engine.time_scale = stop_scale
					Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
					menu_ingame.set_visible(true)
				else:
					Engine.time_scale = play_scale
					Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
					menu_ingame.set_visible(false)
			else:
				get_tree().quit()
		# Fullscreen toggle
		if event.is_action_pressed("r_fullscreen"):
			if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_WINDOWED:
				DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
			else:
				DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		# Reload active scene
		if event.is_action_pressed("sv_reload_scene"):
			get_tree().reload_current_scene()
