extends CharacterBody3D
# character_body.gd
# Defines main character movement

const SPEED = 4.0
const SPRINT = 8.0

# Get project settings
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")
var mouse_sensitivity: float = ProjectSettings.get_setting("character/mouse_sensitivity")
var stop_scale: float = ProjectSettings.get_setting("game/stop_time_scale")
var play_scale: float = ProjectSettings.get_setting("game/play_time_scale")

var move_modifier: float = SPEED
var is_sprinting: bool = false

@onready
var camera_rig: Node3D = $CameraRig


func _ready() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _physics_process(delta: float) -> void:
	# Get the input direction and handle the movement/deceleration.
	var input_dir: Vector2 = Input.get_vector(
			"cl_move_left", 
			"cl_move_right", 
			"cl_move_forward", 
			"cl_move_backward")
	
	var direction: Vector3 = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	if is_sprinting:
		move_modifier = SPRINT
	else:
		move_modifier = SPEED
	
	if direction:
		var accelerration: float = 5 * delta
		velocity.x = lerp(velocity.x, direction.x * move_modifier, accelerration)
		velocity.z = lerp(velocity.z, direction.z * move_modifier, accelerration)
	else:
		var accelerration: float = 10 * delta
		velocity.x = lerp(velocity.x, direction.x * move_modifier, accelerration)
		velocity.z = lerp(velocity.z, direction.z * move_modifier, accelerration)
	
	velocity.y -= gravity * delta
	
	move_and_slide()


func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.is_action_pressed("cl_sprint"):
			is_sprinting = true
		elif event.is_action_released("cl_sprint"):
			is_sprinting = false
	if event is InputEventMouseMotion:
		if Engine.time_scale == stop_scale:
			return
		
		var yaw: float = -event.relative.x / 100
		var pitch: float = -event.relative.y / 100
		
		self.rotate_y(yaw * (mouse_sensitivity / 100))
		camera_rig.rotate_x(pitch * (mouse_sensitivity / 100))
