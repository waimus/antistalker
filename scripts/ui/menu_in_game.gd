extends Control


var stop_scale: float = ProjectSettings.get_setting("game/stop_time_scale")
var play_scale: float = ProjectSettings.get_setting("game/play_time_scale")


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	self.set_visible(false)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass


func _on_button_resume_pressed() -> void:
	if Engine.time_scale == stop_scale:
		Engine.time_scale = play_scale
	
	if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
	self.set_visible(false)


func _on_button_menu_pressed() -> void:
	if Engine.time_scale == stop_scale:
		Engine.time_scale = play_scale
	get_tree().change_scene_to_file("res://nodes/ui/menu_main.tscn")


func _on_button_quit_pressed() -> void:
	get_tree().quit()
