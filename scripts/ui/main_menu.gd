extends Control

@export var level_strings: Array[Resource] = []

var level_selector: int = 0

@onready var cont_levels: Control = $ContLevelSelect
@onready var cont_credit: Control = $ContCredit


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	cont_levels.set_visible(false)
	cont_credit.set_visible(false)
	
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE


func _on_button_level_select_pressed() -> void:
	cont_levels.set_visible(true)


func _on_button_credit_pressed() -> void:
	cont_credit.set_visible(true)


func _on_button_quit_pressed() -> void:
	get_tree().quit()


func _on_button_level_back_pressed() -> void:
	cont_levels.set_visible(false)


func _on_button_level_start_pressed() -> void:
	get_tree().change_scene_to_packed(level_strings[level_selector])


func _on_button_credit_back_pressed() -> void:
	cont_credit.set_visible(false)


func _on_button_level_1_pressed() -> void:
	level_selector = 0


func _on_button_level_2_pressed() -> void:
	level_selector = 1


func _on_button_level_3_pressed() -> void:
	level_selector = 2
