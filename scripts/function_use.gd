extends Node3D


var select_object: RigidBody3D
var pull_acceleration: float = 8.0

@onready var interaction_ray: RayCast3D = $RayCast3D
@onready var pos_hold: Marker3D = $Marker3D


func _physics_process(delta: float) -> void:
	if select_object:
		var a: Vector3 = select_object.global_position
		var b: Vector3 = pos_hold.global_position
		
		select_object.set_linear_velocity((b - a) * pull_acceleration)
		select_object.look_at(self.global_position)


func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.is_action_pressed("cl_fn_use"):
			if !select_object:
				pick_object()
			else:
				drop_object()


func pick_object() -> void:
	var hit: Object = interaction_ray.get_collider()
	if hit and hit is RigidBody3D:
		select_object = hit
		
		if select_object.freeze:
			select_object.freeze = false


func drop_object() -> void:
	if select_object:
		select_object = null
